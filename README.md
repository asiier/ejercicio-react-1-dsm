# Ejercicio React 1 - DSM

**Ejercicio React 1:** Componentes, eventos, estado. De la asignatura de DSM del MUITEL.

## Tareas a realizar

- - [X] 1.  Crear una aplicación React.
- - [X] 2.  Crear un componente con un campo de entrada input.
- - [X] 3.  Crear un segundo componente para mostrar el resultado de multiplicar el número insertado en el componente anterior.
- - [X] 4.  Habrá dos botones. Pulsando el primero, el resultado a mostrar será el número insertado multiplicado por 37. Pulsando el segundo botón, el resultado es el número insertado multiplicado por 43.
- - [X] 5.  Quizás sea recomendable mostrar de alguna manera el botón que está activo, indicando si la multiplicación es por 37 o por 43.

## Como Iniciar

Comandos recomendados para descargar los módulos necesarios e iniciar el proyecto de React de este repositorio. Todos los comandos deberán ejecutar en el directorio de este repositorio.

1. ```npm install``` Instalara todas las dependencias (los 'node modules') necesarios.
2. ```npm start``` Inicializará la aplicación web. Al usar React se ejecutará el comando ```react-scripts start```. 
3. *Opcional* ```npm update``` Actualiza las dependencias a su version mas reciente.
4. *Opcional* ```npm audit fix``` Actualiza aquellas dependencias con vulnerabilidades y fallos de seguridad. **Cuidado** Si se están encontrando pocas (<10) vulnerabilidades y/o su severidad es moderada **NO** ejecutar. Tratará de descargar paquetes más antiguos con más vulnerabilidades.

Si no puede instalar pruebe a borrar el archivos [*'package-lock.json'*](./package-lock.json) y volver al paso 1.

Ejecutar ```npm build``` para crear la aplicación web optimizada para el entorno de producción.


## Documentación de React
### Getting Started with Create React App
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Available Scripts

In the project directory, you can run:

#### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

#### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

#### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

#### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

#### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

#### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

#### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

#### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

#### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

#### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
