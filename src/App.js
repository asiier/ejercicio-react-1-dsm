import React from 'react';
import './App.css';

import Input from './Components/InputComponent/Input';
import Button from './Components/ButtonComponent/Button';
import Display from './Components/DisplayComponent/Display';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      input: 0,
      times: 0,
      value: 0,
      error: false,
      btn37: false,
      btn43: false
    }
  }

  setMult = (mult) => {
    this.setState({times: mult}, () => this.getMult());
    mult === 43 ? this.setState({btn37: false, btn43: true}) : this.setState({btn37: true, btn43: false}, () => this.render());
  }

  getMult = () => {
    this.setState({value: this.state.times * this.state.input});
  }

  setInput = value => {
    let num = !isNaN(Number(value)) ? Number(value) : 0;
    let err = isNaN(Number(value)) ? true : false;
    this.setState({ input: num, error: err }, () => this.getMult());
  }

  render() {
    let displayText = this.state.error ? 'Por favor, introduce un valor correcto.' : `${this.state.input} x ${this.state.times} =  ${this.state.value}`;
    
    return(
      <div className="App">
        <h1>Calculadora</h1>
        <Input call={value=>this.setInput(value)}/>
        <div className='button-wrapper'>
          <Button call={this.setMult} active={this.state.btn37} value={37}/>
          <div style={{padding: "10px"}}></div>
          <Button call={this.setMult} active={this.state.btn43} value={43}/>
        </div>
        <Display value={displayText}/> 
      </div>
    );
  }
}

export default App;
