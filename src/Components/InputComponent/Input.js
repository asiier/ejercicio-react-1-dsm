import React from 'react';

class Input extends React.Component {

    render() {
        return ( 
            <input type={"text"} placeholder="Introduce un número" onChange={event=>this.props.call(event.target.value)}></input>
        )
    }
}

export default Input;