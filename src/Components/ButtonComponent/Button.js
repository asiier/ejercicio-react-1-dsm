import React from 'react';

class Button extends React.Component {

    render() {
        return (
            <button className={[this.props.active ? "active" : "", "button"].join(' ')} 
            onClick={ () => { 
                this.props.call(this.props.value);
            } }>{this.props.value}</button>
        )
    }
}

export default Button;